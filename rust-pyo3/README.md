# Rust Python bindings using PyO3

Example project providing Python bindings for Rust code, using PyO3.

## Structure

```
root
│   <Project configuration>
│   Cargo.toml
│   MANIFEST.in
│   pyproject.toml
│   setup.py
│
├───rust_example
│       <Python entrypoint>
│       __init__.py
│
└───src
        <Rust code, exposing via PyO3>
        lib.rs
```

## Installation

```shell
pip install .
```

## Testing

```python
from rust_example import Calc
Calc().add(2,2)
```

> Run outside the project folder! Otherwise, there will be conflicts with the local Python entrypoint.
