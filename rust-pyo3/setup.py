#!/usr/bin/env python
from setuptools import setup
from setuptools_rust import Binding, RustExtension

setup(
    name="rust_example",
    version="1.0",
    rust_extensions=[RustExtension("rust_example.rust_example", binding=Binding.PyO3)],
    packages=["rust_example"],
    zip_safe=False,
)
