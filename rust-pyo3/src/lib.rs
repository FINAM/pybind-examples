use pyo3::prelude::*;

#[pymodule]
fn rust_example(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_class::<Calc>()?;
    Ok(())
}

#[pyclass]
struct Calc();

#[pymethods]
impl Calc {
    #[new]
    fn new() -> Self {
        Calc()
    }
    
    pub fn add(&self, a: i32, b: i32) -> PyResult<i32> {
        Ok(a + b)
    }
    
    pub fn sub(&self, a: i32, b: i32) -> PyResult<i32> {
        Ok(a - b)
    }
}
