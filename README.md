# Python bindings examples

This repository serves as a reference for FINAM users on how to write Python bindings for their models.

* C++
  * **[bindings for C++](./cpp-cython/)** using [Cython](https://cython.org/)
  * *[bindings for C++](./cpp-pybind/) using [pybind11](https://pybind11.readthedocs.io/en/stable/)* (TODO)
* Fortran
  * *[bindings for Fortran](./fortran-pybind/) using [pybind11](https://pybind11.readthedocs.io/en/stable/)* (TODO)
* Rust
  * **[bindings for Rust](./rust-pyo3/)** using [PyO3](https://github.com/PyO3/pyo3/)
* Java
  * *[bindings for Java](./java-jpipe/) using [JPipe](https://jpype.readthedocs.io/en/latest/)* (TODO)