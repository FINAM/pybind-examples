import sys

from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

extensions = [
    Extension(
        "cpp_cython_example",
        [
            "cpp_cython_example/cpp_cython_example.pyx",
            "cpp_cython_example/src/example.cpp",
        ],
        include_dirs=["cpp_cython_example/src"],
        extra_compile_args=[],
        extra_link_args=[],
        language="c++",
    )
]

setup(ext_modules=cythonize(extensions))
