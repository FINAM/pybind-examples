# coding=utf-8
# distutils: language=c++
#@PydevCodeAnalysisIgnore
#cython: language_level=3
###, boundscheck=False, wraparound=False, nonecheck=False


cdef extern from "example.h":
    cdef cppclass Calc:
        int add(int a, int b) nogil except +
        int sub(int a, int b) nogil except +
