# distutils: language=c++
#cython: language_level=3, boundscheck=False, wraparound=False, nonecheck=False
#@PydevCodeAnalysisIgnore

cdef class CalcWrapper():
    cdef Calc calc

    def add(self, a, b):
        return self.calc.add(a, b)

    def sub(self, a, b):
        return self.calc.sub(a, b)
