# C++ Python bindings using Cython

Example project providing Python bindings for C++ code, using Cython.

## Structure

```
root
│   <Project configuration>
│   MANIFEST.in
│   pyproject.toml
│   setup.cfg
│   setup.py
│
└───cpp_cython_example
    │   <Cython bindings files>
    │   cpp_cython_example.pxd
    │   cpp_cython_example.pyx
    │
    └───src
            <C++ source and header files>
            example.cpp
            example.h
```

## Installation

```shell
pip install .
```

## Testing

```python
from cpp_cython_example import CalcWrapper
CalcWrapper().add(2,2)
```
